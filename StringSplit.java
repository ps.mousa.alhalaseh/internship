package com.ps.internship;

import java.util.Arrays;
import java.util.Scanner;

public class StringSplit {
    static void splitString(String str) {
        StringBuilder alpha = new StringBuilder(), num = new StringBuilder();

        for (int i=0; i<str.length(); i++) {
            if (Character.isDigit(str.charAt(i)))
                num.append(str.charAt(i));
            else if (Character.isAlphabetic(str.charAt(i)))
                alpha.append(str.charAt(i));
        }
        String alphaString = alpha.toString();
        char[] letters =  alphaString.toCharArray();

        String numString = num.toString();
        char[] numbers =  numString.toCharArray();

        for (char element : letters) {
            System.out.println(element);
        }
        System.out.println("---------------");
        for (char element : numbers) {
            System.out.println(element);
        }
        System.out.println("-----------------");
        System.out.println(Arrays.toString(letters));
        System.out.println(Arrays.toString(numbers));

    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String string = input.nextLine();
        splitString(string);
    }



}
