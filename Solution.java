package com.ps.internship;

import java.util.Scanner;

public class Solution {
    private static int ones = 0;

    // Recursion
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int size = input.nextInt();
        int[] groups = new int[size];

        for (int i = 0; i < groups.length; i++) {
            groups[i] = input.nextInt();
            if (groups[i] == 1)
                ones++;
        }

        int result = (int) Math.ceil(calculate(groups, 0));
        System.out.println(result);
    }

    private static double calculate(int[] groups, int index) {
        if (index == groups.length) {
            if (ones > 0 )
                return ones / 4.0;
            return 0;
        }

        if (groups[index] == 4)
            return 1 + calculate(groups, ++index);

        if (groups[index] == 3) {
            ones--;
            return 1 + calculate(groups, ++index);
        }

        if (groups[index] == 2)
            return 0.5 + calculate(groups, ++index);

        if (groups[index] == 1)
            return calculate(groups, ++index);

        return 0;
    }
}
