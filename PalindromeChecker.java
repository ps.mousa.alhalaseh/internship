package com.ps.internship;

import java.util.Scanner;

public class PalindromeChecker {
    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        String stringInput = "";

        Scanner input = new Scanner(System.in);
        stringInput = input.nextLine();
        int length = stringInput.length();

        for(int i = length - 1; i >= 0; i--) {
            builder.append(stringInput.charAt(i));
        }
        if (stringInput.equalsIgnoreCase(builder.toString())) {
            System.out.println("Palindrome");
        }
        else {
            System.out.println("Not Palindrome");
        }
    }
}