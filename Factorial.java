package com.ps.internship;

import java.util.Scanner;

public class Factorial {
    static int factorial(int n){
        if (n==0){
            return 1;
        }
        return (n*factorial(n - 1));
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();
        int f = 1;

        f = factorial(num);
//        Scanner input = new Scanner(System.in);
//        int n = input.nextInt();
//
//        int factorial = 1;
//        for (int i = 1; i <= n; i++) {
//            factorial = factorial*i;
//        }

        System.out.println(f);


    }
}