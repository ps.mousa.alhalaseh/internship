package com.ps.internship;

import java.util.Scanner;

public class Prime {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = 0;
        int flag = 0;
        int number = input.nextInt();
        m = number / 2;
        if (number == 0 || number == 1) {
            System.out.println("not prime");
            return;
        }
        if (isPrime(m, number)) {
            System.out.println("Prime");
            return;
        }
        System.out.println("Not Prime");


    }

    public static boolean isPrime(int m, int number) {
        for (int i = 2; i <= m; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
